import functools
import binascii
import time
import os.path
import datetime
import logging

day = os.path.basename(__file__).split('.')[0]
logging.basicConfig(format='%(asctime)s - %(levelname)s:%(message)s',
                    level=logging.DEBUG,
                    filename="%s.log"%(day,),
                    filemode="w")

class Filter(object):
    def __init__(self, line):
        self.line = line
        assert (len(line) == 512)
    def value (self,s):
        i = 0
        for x in s:
            if x == '#':
                i=2*i + 1
            elif x== '.':
                i=2*i
            else:
                logging.critical ("unexpected character %s in %s" % (x, s))
                assert(False)
        assert (i<len(self.line))
        assert (self.line[0] == '.') # limit growth and prevents blinking
        return self.line[i]

class Grid(object):
    def __init__(self):
        self.data = []
        self.x = 0
        self.y = 0
    def getValue(self,x,y):
        if x<0 or y<0:
            return '.'
        if x >= self.x or y >= self.y:
            return '.'
        return self.data[y][x]



def loadFile(f):
    with open (f,'r') as fh:
        l1 = fh.readline().strip()
        grid =[x.strip() for x in  fh.readlines() if len(x)>0]
    f = Filter(l1)
    g = Grid()
    return f,l

def test():
    f = "%s_test.txt" % (day,)
    fil,l = loadFile(f)
    assert(fil.value('...#...#..') == '#')

def main():
    f = "%s.txt" % (day,)


def run():
    t1 = datetime.datetime.now()
    test()
    t2 = datetime.datetime.now()
    logging.debug("test took %s" % (str(t2-t1)))
    main()
    t3 = datetime.datetime.now()
    logging.debug ("run took %s" % (str(t3-t2)))


run()
