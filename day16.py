import functools
import binascii

CONV = {
    '0':'0000',
    '1':'0001',
    '2':'0010',
    '3':'0011',

    '4':'0100',
    '5':'0101',
    '6':'0110',
    '7':'0111',

    '8':'1000',
    '9':'1001',
    'A':'1010',
    'B':'1011',

    'C':'1100',
    'D':'1101',
    'E':'1110',
    'F':'1111',
}

if True:
    s="D2FE28"
else:
    f="day15.txt"

def binToVal(x):
    r = 0
    for y in x:
        r*=2
        if y =='1':
            r+=1
    return r

class Packet(object):
    def __init__(self, ver):
        self.version = binToVal(ver)

class Literal(Packet):
    def __init__(self, ver, p):
        super(Literal,self).__init__(ver)
        self.value = binToVal(self.parsePayload(p))
    def parsePayload(self, p):
        v=[]
        while len(p) >=5 :
            v.append(p[1:5])
            keep = p[0]
            p=p[5:]
            if keep == '0':
                break
        self.remain = p
        return ''.join(v)
    def getVersion(self):
        return self.version
    def getValue(self):
        return self.value

class Operand(Packet):
    def __init__(self,ver, tag, payload):
        super(Operand,self).__init__(ver)
        self.tag =binToVal(tag)
        self.children = []
        self.processPayload(payload)
    def processPayload(self,payload):
        length_id = payload[0]
        if length_id=='0':
            length=payload[1:16]
            r = payload[16:16+binToVal(length)]
            self.remain=payload[16+binToVal(length):]
            while True:
                p, r = parse(r)
                self.children.append(p)
                if len(r)<=6:
                    break
        else:
            nb=binToVal(payload[1:1+11])
            r=payload[12:]
            for x in range(0,nb):
                p,r =  parse(r)
                self.children.append(p)
            self.remain = r
    def getVersion(self):
        return self.version+sum((x.getVersion() for x in self.children))
    def getValue(self):
        return {
            0:lambda z:sum((y.getValue() for y in z)),
            1:lambda z:functools.reduce(lambda x,y:x*y.getValue(),z,1),
            2:lambda z:min((y.getValue() for y in z)),
            3:lambda z:max((y.getValue() for y in z)),

            5:lambda z:int(z[0].getValue()>z[1].getValue()),
            6:lambda z:int(z[0].getValue()<z[1].getValue()),
            7:lambda z:int(z[0].getValue()==z[1].getValue()),
        }[self.tag](self.children)




    


def parse(s):
    ver = s[0:3]
    tag = s[3:6]
    payload = s[6:]
    if tag == '100':
        packet = Literal(ver,payload)
    else:
        packet = Operand(ver, tag, payload)
    return packet, packet.remain

def computeHex(hx):
    return parse(''.join((CONV[x] for x in hx)))

def test():
    p, r = computeHex('D2FE28')
    assert (r=='000')
    assert(p.value == 2021)
    p, r = computeHex('38006F45291200')
    assert(p.version == 1)
    assert(len(p.children) == 2)
    p, r = computeHex('EE00D40C823060')
    assert(p.version == 7)
    assert(p.tag == 3)
    assert(len(p.children) == 3)
    assert(len(r) == 5)
    p, r = computeHex('8A004A801A8002F478')
    assert(p.getVersion() == 16)
    p, r = computeHex('620080001611562C8802118E34')
    assert(p.getVersion() == 12)
    p, r = computeHex('C0015000016115A2E0802F182340')
    assert(p.getVersion() == 23)
    p, r = computeHex('A0016C880162017C3686B18A3D4780')
    assert(p.getVersion() == 31)

    p, r = computeHex('C200B40A82')
    assert (p.getValue() == 3)
    p, r = computeHex('04005AC33890')
    assert (p.getValue() == 54)

    p, r = computeHex('880086C3E88112')
    assert (p.getValue() == 7)
    p, r = computeHex('CE00C43D881120')
    assert (p.getValue() == 9)


    p, r = computeHex('D8005AC2A8F0')
    assert (p.getValue() == 1)
    p, r = computeHex('F600BC2D8F')
    assert (p.getValue() == 0)
    p, r = computeHex('9C005AC2F8F0')
    assert (p.getValue() == 0)
    p, r = computeHex('9C0141080250320F1802104A08')
    assert (p.getValue() == 1)

def real():
    s = '220D6448300428021F9EFE668D3F5FD6025165C00C602FC980B45002A40400B402548808A310028400C001B5CC00B10029C0096011C0003C55003C0028270025400C1002E4F19099F7600142C801098CD0761290021B19627C1D3007E33C4A8A640143CE85CB9D49144C134927100823275CC28D9C01234BD21F8144A6F90D1B2804F39B972B13D9D60939384FE29BA3B8803535E8DF04F33BC4AFCAFC9E4EE32600C4E2F4896CE079802D4012148DF5ACB9C8DF5ACB9CD821007874014B4ECE1A8FEF9D1BCC72A293A0E801C7C9CA36A5A9D6396F8FCC52D18E91E77DD9EB16649AA9EC9DA4F4600ACE7F90DFA30BA160066A200FC448EB05C401B8291F22A2002051D247856600949C3C73A009C8F0CA7FBCCF77F88B0000B905A3C1802B3F7990E8029375AC7DDE2DCA20C2C1004E4BE9F392D0E90073D31634C0090667FF8D9E667FF8D9F0C01693F8FE8024000844688FF0900010D8EB0923A9802903F80357100663DC2987C0008744F8B5138803739EB67223C00E4CC74BA46B0AD42C001DE8392C0B0DE4E8F660095006AA200EC198671A00010E87F08E184FCD7840289C1995749197295AC265B2BFC76811381880193C8EE36C324F95CA69C26D92364B66779D63EA071008C360098002191A637C7310062224108C3263A600A49334C19100A1A000864728BF0980010E8571EE188803D19A294477008A595A53BC841526BE313D6F88CE7E16A7AC60401A9E80273728D2CC53728D2CCD2AA2600A466A007CE680E5E79EFEB07360041A6B20D0F4C021982C966D9810993B9E9F3B1C7970C00B9577300526F52FCAB3DF87EC01296AFBC1F3BC9A6200109309240156CC41B38015796EABCB7540804B7C00B926BD6AC36B1338C4717E7D7A76378C85D8043F947C966593FD2BBBCB27710E57FDF6A686E00EC229B4C9247300528029393EC3BAA32C9F61DD51925AD9AB2B001F72B2EE464C0139580D680232FA129668'
    p,r=computeHex(s)
    print ("star1",p.getVersion())
    print ("star2",p.getValue())


test()
real()
