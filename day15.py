import functools
import bisect


if False:
    f="day15_test.txt"
else:
    f="day15.txt"


def LoadFile(f):
    ret = dict()
    with open(f,"r") as fh:
        for y,line in enumerate(fh.readlines()):
            for x,value in enumerate((int(z) for z in line.strip())):
                ret[(x,y)] = value
    return ret,x,y


def FindPath(maze):
    result = {(0,0):(0,None)}
    consider = {(0,0)}
    while len(consider) > 0:
        reconsider= set()
        for x,y in consider:
            cost, _ = result[(x,y)]
            for xx, yy in ((0,1),(0,-1),(-1,0),(1,0)):
                lx = x + xx
                ly = y + yy
                if (lx,ly) not in maze:
                    continue
                new_cost = cost + maze[(lx,ly)]

                keep = False
                if (lx,ly) in result:
                    old_cost = result[(lx,ly)][0]
                    if new_cost < old_cost:
                        keep = True
                else:
                    keep = True
                if keep:
                    result[(lx,ly)] = (new_cost,(x,y))
                    reconsider.add((lx,ly))
        consider=reconsider
    return result

def FindPathSmart(maze,end):
    result = {(0,0):(0,None)}
    consider = [(0,0)]
    cnt = 0
    while (not end in result):
        cnt+=1
        x,y=consider.pop(0)
        cost, _ = result[(x,y)]
        for xx, yy in ((0,1),(0,-1),(-1,0),(1,0)):
            lx = x + xx
            ly = y + yy
            if (lx,ly) not in maze:
                continue
            new_cost = cost + maze[(lx,ly)]

            keep = False
            if (lx,ly) in result:
                old_cost = result[(lx,ly)][0]
                if new_cost < old_cost:
                    keep = True
            else:
                keep = True
            if keep:
                result[(lx,ly)] = (new_cost,(x,y))
                if len(consider) == 0:
                    consider=[(lx,ly)]
                else:
                    consider.append((lx,ly))
                    consider.sort(key=lambda z:result[z][0])
    return result


def upgradeMaze(maze,x1,y1):
    ret = dict()
    for mulx in range(0,5):
        for muly in range(0,5):
            for x in range(0,x1+1):
                for y in range(0,y1+1):
                    ret[(x+mulx*(x1+1),y+muly*(y1+1))] = ((maze[(x,y)] + mulx + muly - 1) % 9) +1
    return ret,5*(x1+1)-1,5*(y1+1)-1



maze,x,y = LoadFile(f)

def printPath(maze,cost,current):
    if current is None:
        return
    lcost,previous = cost[(current)]
    printPath(maze, cost, previous)
    print (current, lcost, maze[current])

naive=False

if naive:
    costs = FindPath(maze)
else:
    costs = FindPathSmart(maze,(x,y))

print ("star1",costs[(x,y)])

# printPath(maze,costs,(x,y))

maze2,x2,y2 = upgradeMaze(maze,x,y)

if naive:
    costs2 = FindPath(maze2)
else:
    costs2 = FindPathSmart(maze2,(x2,y2))

print ("star2",costs2[(x2,y2)])
