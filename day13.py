import collections
import functools

if False:
    f="day13_test.txt"
else:
    f="day13.txt"


def  LoadFile(f):
    ret  = set()
    inst = []
    with open(f,'r') as fh:
        for l in fh.readlines():
            t = l.strip().split(",")
            if len(t) == 2:
                ret.add ((int(t[0]),int(t[1])))
                continue
            t=l.strip().split("=")
            if len(t) == 2:
                inst.append((t[0][-1],int(t[1])))
    return (ret, inst)


def fold(dots, instruction):
    n = set()
    for x,y in dots:
        offset = instruction[1]
        if instruction[0] == 'x':
            if x < offset:
                n.add((x,y))
            if x == offset:
                # in fold
                pass
            if x > offset:
                n.add((2*offset-x,y))
        elif instruction[0] == 'y':
            if y < offset:
                n.add((x,y))
            if y == offset:
                # in fold
                pass
            if y > offset:
                n.add((x,2*offset-y))
        else:
            print (dots, instruction)
            assert (False)
    return n

dots,instructions = LoadFile(f)


def display(s):
    x1 = max((x[0]+1 for x in s))
    y1 = max((x[1]+1 for x in s))
    for y in range(0,y1):
        l=[]
        for x in range(0,x1):
            if (x,y) in s:
                l.append('X')
            else:
                l.append(' ')
        print (''.join(l))

s2 = fold(dots, instructions[0])

s=dots
for i in instructions:
    s = fold(s, i)


print ("star1", len(s2))
display(s)
