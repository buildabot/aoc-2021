import collections
import functools

if False:
    f="day14_test.txt"
else:
    f="day14.txt"


def  LoadFile(f):
    rules = dict()
    with open(f,'r') as fh:
        base = [x for x in fh.readline().strip()]
        for l in fh.readlines():
            t = l.strip().split(" -> ")
            if len(t) ==2:
                a,b = t
                rules[(a[0],a[1])] = b
    return base, rules


def iterate(base, rules):
    ret = [base[0]]
    for x in range(0,len(base)-1):
        l1, l2 = base[x],base[x+1]
        if (l1,l2) in rules:
            ret.append(rules[(base[x],base[x+1])])
        ret.append(l2)
    return ret

def findResult(base):
    cnt=collections.Counter()
    for x in base:
        cnt[x]+=1
    b = [x for x in cnt.keys()]
    b.sort (key = lambda x:cnt[x])
    return cnt[b[-1]]-cnt[b[0]]

def findResult2(base, extremities):
    cnt=collections.Counter()
    cnt[extremities[0]]+=1
    cnt[extremities[1]]+=1
    for k,v in base.items():
        a,b = k
        cnt[a]+=v
        cnt[b]+=v
    b = [x for x in cnt.keys()]
    b.sort (key = lambda x:cnt[x])
    return int((cnt[b[-1]]-cnt[b[0]]) /2)


def iterate2(base, rules):
    ret = collections.Counter()
    for (a,b),v in base.items():
        if (a,b) in rules:
            c = rules[(a,b)]
            ret[(a,c)]+=v
            ret[(c,b)]+=v
        else:
            ret[(a,b)]+=v
    return ret
    

base,rules = LoadFile(f)
extremities = (base[0],base[-1])
pairs = collections.Counter()
for x in range(0,len(base)-1):
    pairs[(base[x],base[x+1])]+=1


for x in range(0,10):
    base = iterate(base, rules)
    pairs = iterate2(pairs, rules)
    r1 = findResult(base)
    r2 = findResult2(pairs, extremities)
    assert (r1 == r2)

print (findResult(base))

for x in range(0,30):
    pairs = iterate2(pairs, rules)

print (findResult2(pairs, extremities))

