import functools
import binascii
import time
import os.path
import datetime
import logging

day = os.path.basename(__file__).split('.')[0]
logging.basicConfig(format='%(asctime)s - %(levelname)s:%(message)s',
                    level=logging.DEBUG,
                    filename="%s.log"%(day,),
                    filemode="w")

class Board(object):
    def __init__ (self,size):
        self.size = size
    def move (self,position,rolls):
        d =  sum(rolls)
        new_position = ((position - 1) + d) % self.size +1
        logging.debug("from %s, rolls %s and scores %s" % (position, ','.join((str(x) for x in rolls)), new_position))
        return new_position

class Die(object):
    def __init__(self):
        self.size = 100
        self.current = 1
        self.rolls=0
    def roll(self):
        self.rolls+=1
        r = self.current
        self.current +=1
        if self.current == 101:
            self.current = 1
        return r

class Player(object):
    def __init__ (self, position, board):
        self.position = position
        self.score = 0
        self.board = board
    def play (self,dice):
        self.position = self.board.move(self.position, [dice.roll() for x in range(0,3)])
        self.score += self.position

class Game(object):
    def __init__ (self, p1, p2):
        self.board = Board(10)
        self.die = Die()
        self.players=[Player(p1,self.board),Player(p2,self.board)]
        self.swap={0:1,1:0}
    def play(self,limit):
        current = 0
        while True:
            p = self.players[current]
            p.play(self.die)
            logging.debug(p.score)
            if p.score >= limit:
                logging.debug ("player 1 score %u" % self.players[0].score)
                logging.debug ("player 2 score %u" % self.players[1].score)
                logging.debug ("Die rolled %u times" % self.die.rolls)
                return self.players[self.swap[current]].score * self.die.rolls
            current = self.swap[current]


def test():
    f = "%s_test.txt" % (day,)
    d = Board(10)
    assert (d.move(4,[1,2,3]) == 10)

    g= Game(4,8)
    r = g.play(1000)
    assert (r == 739785)

def main():
    f = "%s.txt" % (day,)
    g= Game(7,9)
    r = g.play(1000)
    print ("star 1 : %u" % (r,))


def run():
    t1 = datetime.datetime.now()
    test()
    t2 = datetime.datetime.now()
    logging.debug("test took %s" % (str(t2-t1)))
    main()
    t3 = datetime.datetime.now()
    logging.debug ("run took %s" % (str(t3-t2)))


run()
