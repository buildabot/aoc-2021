import functools

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--debug', action=argparse.BooleanOptionalAction)
args = parser.parse_args()



if args.debug:
    f="day9_test.txt"
else:
    f="day9.txt"


DIRECTIONS=((1,0),(-1,0),(0,1),(0,-1))

class HeightMap(object):
    def __init__(self,data):
        self.data = data
        self.mini = set()
    def getValue(self,x,y):
        if x>=0 and x<len(self.data):
            if y>=0 and y<len(self.data[0]):
                return self.data[x][y]
        return None
            
    def getNeighbors (self,x,y):
        ret = [self.getValue(x+xx,y+yy) for(xx,yy) in DIRECTIONS]
        r = [a for a in ret if a is not None]

        return r
    def computeMini(self):
        for x in range (0, len(self.data)):
            for y in range (0,len(self.data[0])):
                a = [self.data[x][y]<v for v in self.getNeighbors(x,y)]
                if functools.reduce(lambda x,y:x and y, a):
                    self.mini.add((x,y))
    def computeBasin(self,x,y,processed,value):
        if (x,y) in processed:
            return 0
        current = self.getValue(x,y)
        if current is None:
            return 0
        if  current == 9:
            return 0
        ret = 1
        processed.add((x,y))

        for xx,yy in DIRECTIONS:
            ret += self.computeBasin(x+xx,y+yy,processed,current)
        return ret
            
            
        
    def star1(self):
        self.computeMini()
        return sum((1+self.data[x][y] for x,y in self.mini))
    
    def star2(self): # assume star1 has been called :D
        processed = set()
        basins = []
        for x,y in self.mini:
            current = self.getValue(x,y)
            basins.append( self.computeBasin(x,y,processed,current-1))
        v = functools.reduce(lambda x,y: x*y, sorted(basins,reverse=True)[0:3])
        return v



with open(f,"r") as fh:
    t = [[int(x) for x in y.strip()] for y in fh.readlines()]


h = HeightMap(t)

print ("Star 1", h.star1())
print ("Star 1", h.star2())
