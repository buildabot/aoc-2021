import collections
import functools

if False:
    l1=len('10011')
    f="day3_test.txt"
else:
    l1=len('100111101000')
    f="day3.txt"

l=list()

with open(f,"r") as fh:
    for x in fh.readlines():
        x2=x.strip()
        l.append(x2)

def find_frequencies(l,i):
    c=collections.Counter()
    for x in l:
        c[x[i]]+=1
    return c
        

gamma=[find_frequencies(l,p).most_common()[0][0]  for p in range(0, l1)]
epsilon=[find_frequencies(l,p).most_common()[-1][0]  for p in range(0, l1)]
print (gamma)
print (epsilon)


oxygen =[x for x in l]
for p in range(0,l1):
    c=find_frequencies(oxygen,p)
    if c['1']>=c['0']:
        oxygen=[x for x in oxygen if x[p]=='1']
    else:
        oxygen=[x for x in oxygen if x[p]=='0']
 

co2 =[x for x in l]
for p in range(0,l1):
    c=find_frequencies(co2,p)
    if c['0']<=c['1']:
        co2=[x for x in co2 if x[p]=='0']
    else:
        co2=[x for x in co2 if x[p]=='1']
    if len(co2)==1:
        break


print (oxygen,co2)

def list_to_int(p):
  r=0
  for x in p:
      r=r*2+int(x)
  return r
    
assert(len(oxygen)==1)
assert(len(co2)==1)

print (c,gamma,epsilon)
g=list_to_int(gamma)
e=list_to_int(epsilon)
o=list_to_int(oxygen[0])
c=list_to_int(co2[0])


print (g,e,g*e,o,c,o*c)



