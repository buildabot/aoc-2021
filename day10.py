import functools



if False:
    f="day10_test.txt"
else:
    f="day10.txt"

OPEN={'(':')','[':']','{':'}','<':'>'}
CLOSE={')':3,']':57,'}':1197,'>':25137}
AUTO={'(':1,'[':2,'{':3,'<':4}

with open(f,"r") as fh:
    data = [x.strip() for x in fh.readlines()]

def correct(l):
    temp = []
    for x in l:
        if x in OPEN.keys():
            temp.append(x)
        if x in CLOSE.keys():
            y=temp.pop()
            if x != OPEN[y]:
                return (False,CLOSE[x])
    ret = 0
    for x in reversed(temp):
        ret = 5*ret + AUTO[x]
    return (True,ret)


assert(correct("{([(<{}[<>[]}>{[]{[(<()>") == (False,1197))
assert(correct("<{([{{}}[<[[[<>{}]]]>[]]") == (True,294))

c = [correct(x) for x in data]
k = [value for correct,value in c if correct]
k.sort()

print ( sum([value for correct,value in c if not correct]), k[int((len(k)-1)/2)])
