if False:
    f="day8_test.txt"
else:
    f="day8.txt"


MAPPING={
    0:{'a','b','c','e','f','g'},
    1:{'c','f'},
    2:{'a','c','d','e','g'},
    3:{'a','c','d','f','g'},
    4:{'b','c','d','f'},
    5:{'a','b','d','f','g'},
    6:{'a','b','d','e','f','g'},
    7:{'a','c''f'},
    8:{'a','b','c','d','e','f','g'},
    9:{'a','b','c','d','f','g'},
}


class Solver(object):
    def problem1(self, statement):
        pb = statement[1]
        return sum((1 for x in pb if len(x) in (2,3,4,7)))
    def problem2(self, statement):
        mapping = {}
        m = {frozenset((y for y in x)) for x in statement[0]}
        def find(m,l,result,target):
            m2 = [x for x in m if len(x) == l]
            assert(len(m2) == 1)
            result[target] = m2[0]
            m.remove(m2[0])

        find (m,2,mapping,1)
        find (m,3,mapping,7)
        find (m,7,mapping,8)
        find (m,4,mapping,4)

        m2 = [x for x in m if mapping[4].issubset(x)]
        assert (len(m2)==1)
        mapping[9]=m2[0]
        m.remove(m2[0])

        m2 = [(len(x),x) for x in m if mapping[7].issubset(x)]
        assert (len(m2)==2)
        for a,b in m2:
            mapping[{6:0,5:3}[a]]=b
            m.remove(b)

        find (m,6,mapping,6)

        m2 = [(len(mapping[4].intersection(x)),x) for x in m]
        assert (len(m2)==2)
        for a,b in m2:
            mapping[{3:5,2:2}[a]]=b
            m.remove(b)

        lookup=dict()
        for x,y in mapping.items():
            lookup[y]=x

        ret = 0
        for x in [lookup[frozenset((y for y in x))] for x in statement[1]]:
            ret*=10
            ret+=x

        return ret


def  LoadFile(f):
    ret  = []
    with open(f,'r') as fh:
        for l in fh.readlines():
            print (l)
            (m, p) = (y.strip() for y in l.strip().split('|'))
            m2 = [x for x in m.split(' ')]
            pb = [x for x in p.split(' ')]
            ret.append((m2,pb))
    return ret

r = LoadFile(f)

total1,total2=0,0
for x in r:
    s = Solver()
    r1 = s.problem1(x)
    r2 = s.problem2(x)
    total1+=r1
    total2+=r2
    print (x,r1,total1,total2)

print ("Star1",total1)
print ("Star2",total2)
