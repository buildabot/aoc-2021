import functools
import binascii
import time
import os.path
import datetime
import logging

day = os.path.basename(__file__).split('.')[0]
logging.basicConfig(format='%(asctime)s - %(levelname)s:%(message)s',
                    level=logging.DEBUG,
                    filename="%s.log"%(day,),
                    filemode="w")


class AluError(Exception):
    pass

class NoMoreInputError(Exception):
    pass

class Alu(object):
    def __init__(self):
        self.commands = {'add':self.add,
                         'mul':self.mul,
                         'div':self.div,
                         'mod':self.mod,
                         'eql':self.eql,
                         'inp':self.inp}
        self.reset()
    def reset(self):
        self.registers = {x:0 for x in 'wxyz'}
        self.inputs = []

    def getValue(self, value):
        if value in self.registers:
            return self.registers[value]
        return int(value)

    def inp(self, a):
        if len(self.inputs) == 0:
            raise NoMoreInputError
        self.registers[a] = self.inputs.pop(0)
    def add(self, a, b):
        self.registers[a] += self.getValue(b)
    def mul(self, a, b):
        self.registers[a] *= self.getValue(b)
    def div(self, a, b):
        d = self.getValue(b)
        if d == 0:
            logging.warning ("Division by 0: %s - %s" % (a,b))
            raise AluError("Division by 0")
        aa = self.registers[a]
        r = aa // d
        # logging.debug("%s (%s) / %s (%s) -> %s" % (a,aa,b,d,r))
        self.registers[a]  = r
    def mod(self, a, b):
        d = self.getValue(b)
        if d == 0:
            logging.warning ("Modulo by 0: %s - %s" % (a,b))
            raise AluError("Division by 0")
        r = self.registers[a] % d
        self.registers[a]  = r
    def eql(self, a, b):
        d = self.getValue(b)
        if self.registers[a] == d:
            self.registers[a] = 1
        else:
            self.registers[a]  = 0

    def run(self, lines, inputs = [], debug = False):
        self.inputs = inputs
        for x in lines:
            if debug:
                reg = {x:y for x,y in self.registers.items()}
            if not(x.startswith ('#')):
                v = x.strip().split (' ')
            self.commands[v[0]](*v[1:])
            if debug:
                print ("%s - %s - %s" %(reg, x, self.registers))


def test():
    f = "%s_test.txt" % (day,)

    alu = Alu()
    alu.run (["add w 1","add z w"])

    assert (alu.registers['w'] == 1)
    assert (alu.registers['z'] == 1)

    invert = ['inp x','mul x -1']
    alu.run(invert, [0])
    assert (alu.registers['x'] == 0)

    alu.run(invert, [-2])
    assert (alu.registers['x'] == 2)

    alu.run(invert, [3])
    assert (alu.registers['x'] == -3)

    res=False
    try:
        alu.run(invert, [])
    except NoMoreInputError:
        res = True
    assert (res)

    res=False
    try:
        alu.reset()
        alu.run(['inp w','div w 0','add z 5'], [42])
    except AluError:
        res = True
    assert (res)
    assert (alu.registers['z'] == 0)

    res=False
    try:
        alu.reset()
        alu.run(['inp w','mod w 0','add z 5'], [42])
    except AluError:
        res = True
    assert (res)
    assert (alu.registers['z'] == 0)

    alu.run(invert, [1,2])
    assert (alu.inputs == [2])

    tester = ["inp z","inp x","mul z 3","eql z x"]
    alu.run(tester, [1,3])
    assert (alu.registers['z'] == 1)
    alu.run(tester, [1,2])
    assert (alu.registers['z'] == 0)
    alu.run(tester, [-1,3])
    assert (alu.registers['z'] == 0)

    bitset = ["inp w","add z w","mod z 2","div w 2","add y w","mod y 2","div w 2","add x w","mod x 2","div w 2","mod w 2"]
    for x in range(0,16):
        alu.reset()
        alu.run(bitset, [x])
        y = x % 2
        z = x//2
        assert (alu.registers['z'] == y)
        y = z % 2
        z = z//2
        assert (alu.registers['y'] == y)
        y = z % 2
        z = z//2
        assert (alu.registers['x'] == y)
        y = z % 2
        assert (alu.registers['w'] == z)

        t="""inp w
mul x 0
add x z
mod x 26
div z 1
add x 10
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 1
mul y x
add z y""".split("\n")
    a = Alu()
    a.run(t,[3],debug=True)
    print (a.registers)

def main():
    # reverse engineering in https://docs.google.com/spreadsheets/d/1pYx3x3cOdz1yzmQ1lFIsgRE4OGLbw76-D0tv5qFbvTw/edit#gid=0
    f = "%s.txt" % (day,)
    with open(f, 'r') as fh:
        prog=[x.strip() for x in fh.readlines() if len(x.strip())>0]


    print (len(prog))
    #find lines that are different from block 1
    for x in range (0,18):
        for y in range (0,14):
            if prog[x] != prog[x+18*y]:
                print (x,y,prog[x],prog[x+18*y])


    #offset 4,5 and 15 is where magic happens
    r = []
    for x in range (0,14):
        if prog[x*18+4] == 'div z 1':
            inc = prog[x*18+15].split(' ')[-1]
            r.append('D%s + %s' % (x,inc))
        elif prog[x*18+4] == 'div z 26':
            inc = str(-int(prog[x*18+5].split(' ')[-1]))
            d = r.pop()
            dd = 'D%s + %s' % (x,inc)
            print ("%s == %s" % (d,dd))
        else:
            assert(False)
    assert (len(r) ==0)



    inp = [9,9,9,9,9,7,9,5,9,1,9,4,5,6]

    print ("star 1 : %s" % (''.join((str(x) for x in inp))))
    n = [1 for x in range(0,14)]
    a = Alu()
    a.run (prog,inp)
    print (a.registers)
    assert (a.registers['z'] == 0)


    inp=[4,5,3,1,1,1,9,1,5,1,6,1,1,1]
    print ("star 2 : %s" % (''.join((str(x) for x in inp))))
    a = Alu()
    a.run (prog,inp)
    print (a.registers)
    assert (a.registers['z'] == 0)

def run():
    logging.debug ("happy start")
    t1 = datetime.datetime.now()
    test()
    t2 = datetime.datetime.now()
    logging.debug("test took %s" % (str(t2-t1)))
    main()
    t3 = datetime.datetime.now()
    logging.debug ("run took %s" % (str(t3-t2)))


run()
