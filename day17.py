import functools
import binascii
import time

if True:
    target = ((20,30),(10,-5))
else:
    target = ((175,227),(-134,-79))


class Position(object):
    def __init__(self, t):
        self.x,self.y = t
    def addVelocity(self, t):
        self.x += t.x
        self.y += t.y
    def __str__(self):
        return ("(%u,%u)" % (self.x, self.y))

class Velocity(object):
    def __init__(self, t):
        self.x, self.y = t
    def drag(self):
        if self.x > 0:
            self.x -= 1
        self.y -=1
    def __str__(self):
        return ("(%u,%u)" % (self.x, self.y))

class Target(object):
    NOT_YET,HIT,TOO_SHORT,TOO_LONG,OVER="NOT_YET,HIT,TOO_SHORT,TOO_LONG,OVER".split(",")
    def __init__(self, t1, t2):
        self.x1,self.x2 = min(t1),max(t1)
        self.y1,self.y2 = min(t2),max(t2)
    def test(self,p):
        if p.x > self.x2:
            return Target.TOO_LONG
        if p.y < self.y1:
            if p.x < self.x1:
                return Target.TOO_SHORT
            else:
                return Target.OVER
        if p.x >= self.x1 and p.x <= self.x2 and p.y >= self.y1 and p.y <= self.y2:
            return Target.HIT
        return Target.NOT_YET
    def deltaY (self):
        return self.y2-self.y1


class Probe(object):
    def __init__(self, velocity):
        self.position=Position((0,0))
        self.velocity=Velocity(velocity)
        self.max_elevation = 0
    def shoot(self,target):
        while True:
            self.position.addVelocity(self.velocity)
            self.velocity.drag()
            self.max_elevation=max(self.max_elevation, self.position.y)
            r = target.test(self.position)
            if r != Target.NOT_YET:
                break
        return r

class Finder(object):
    def __init__(self,t):
        self.target = t
        self.found=set()
    def search(self):
        x0,y0 = (0,self.target.y1)
        while True:
            p = Probe((x0,y0))
            r=p.shoot(self.target)
            if r==Target.TOO_SHORT:
                if p.velocity.x == 0:
                    # no way to ever reach, need more horizontal velocity
                    x0+=1
                else:
                    #try shooting higher
                    y0+=1
            if r==Target.OVER:
                y0+=1

            if r == Target.HIT:
                break
        x1,y1 = x0,y0
        self.found.add((x0,y0))
        #raise Exception()
        m = p.max_elevation
        while True:
            y1=self.target.y1
            while True:
                p = Probe((x1,y1))
                r=p.shoot(self.target)
                if r == Target.HIT:
                    self.found.add((x1,y1))
                    if p.max_elevation>m:
                        m =p.max_elevation
                    y1+=1
                    continue
                if abs(p.velocity.y)  > 3*self.target.deltaY() :
                    break
                y1+=1
            if x1 > self.target.x2:
                break
            x1+=1
        return m




def test():
    t = Target((20,30),(-10,-5))
    a = Probe((7,2))
    r = a.shoot(t)
    assert (r == Target.HIT)
    a = Probe((6,3))
    r = a.shoot(t)
    assert (r == Target.HIT)

    a = Probe((9,0))
    r = a.shoot(t)
    assert (r == Target.HIT)

    a = Probe((17,-4))
    r = a.shoot(t)
    assert (r != Target.HIT)

    t = Target((20,30),(-10,-5))
    f = Finder(t)
    m = f.search()
    assert (m==45)
    print (len(f.found))
    expected = set((
(23,-10),(25,-9),(27,-5),(29,-6),(22,-6),(21,-7),(9,0),(27,-7),(24,-5),
(25,-7),(26,-6),(25,-5),(6,8),(11,-2),(20,-5),(29,-10),(6,3),(28,-7),
(8,0),(30,-6),(29,-8),(20,-10),(6,7),(6,4),(6,1),(14,-4),(21,-6),
(26,-10),(7,-1),(7,7),(8,-1),(21,-9),(6,2),(20,-7),(30,-10),(14,-3),
(20,-8),(13,-2),(7,3),(28,-8),(29,-9),(15,-3),(22,-5),(26,-8),(25,-8),
(25,-6),(15,-4),(9,-2),(15,-2),(12,-2),(28,-9),(12,-3),(24,-6),(23,-7),
(25,-10),(7,8),(11,-3),(26,-7),(7,1),(23,-9),(6,0),(22,-10),(27,-6),
(8,1),(22,-8),(13,-4),(7,6),(28,-6),(11,-4),(12,-4),(26,-9),(7,4),
(24,-10),(23,-8),(30,-8),(7,0),(9,-1),(10,-1),(26,-5),(22,-9),(6,5),
(7,5),(23,-6),(28,-10),(10,-2),(11,-1),(20,-9),(14,-2),(29,-7),(13,-3),
(23,-5),(24,-8),(27,-9),(30,-7),(28,-5),(21,-10),(7,9),(6,6),(21,-5),
(27,-10),(7,2),(30,-9),(21,-8),(22,-7),(24,-9),(20,-6),(6,9),(29,-5),
(8,-2),(27,-8),(30,-5),(24,-7)))
    assert (len(expected) == len (f.found))

def main():
    t = Target((175,227),(-134,-79))
    f = Finder(t)
    m = f.search()
    print    ("star 1: %u" % m)
    print    ("star 2: %u" % len(f.found))

test()
main()
