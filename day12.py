import collections
import functools

if False:
    f="day12_test.txt"
else:
    f="day12.txt"


network=collections.defaultdict(list)

@functools.lru_cache
def isLarge(location):
    return (location.upper() == location)


def findPath(start, network, current, star2 = False):
    retVal = []
    for k in network[start]:
        curPath = current[:]
        curPath.append(k)
        if k == 'end':
            retVal.append(curPath)
            continue
        if k== 'start':
            continue
        if isLarge(k):
            retVal += findPath(k, network, curPath, star2)
        else:
            cnt  = collections.Counter()
            for x in current:
                if not isLarge(x):
                    cnt[x]+=1

            if star2:
                if 2 in cnt.values():
                    skip = cnt[k] > 0
                else:
                    skip = cnt[k] > 1
            else:
                skip = cnt[k] > 0

            if skip:
                continue
            else:
                retVal += findPath(k, network, curPath, star2)
    return retVal

def  LoadFile(f):
    ret  = []
    with open(f,'r') as fh:
        for l in fh.readlines():
            t = l.strip().split("-")
            ret.append(t)
    return ret

r = LoadFile(f)

for e1,e2 in r:
    network[e1].append(e2)
    network[e2].append(e1)

a = findPath('start',network,['start'])
print ("star 1", len(a)) 

b = findPath('start',network,['start'],True)

print ("star 2", len(b))

