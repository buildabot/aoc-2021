import re
import collections

if False:
    f="day5_test.txt"
else:
    f="day5.txt"


class Line(object):
    def __init__ (self, data):
        self.HORIZONTAL,self.VERTICAL,self.OTHER=range(0,3)
        result = re.findall(r'(\d+),(\d+)', data)
        self.coord1 = [int(x) for x in result[0]]
        self.coord2 = [int(x) for x in result[1]]
        if self.coord1[0] == self.coord2[0]:
            self.lineType = self.HORIZONTAL
        elif self.coord1[1] == self.coord2[1]:
            self.lineType = self.VERTICAL
        else:
            self.lineType = self.OTHER
    def getLine(self, diag = False):
        tx = (self.coord1[0],self.coord2[0])
        ty = (self.coord1[1],self.coord2[1])
        if self.lineType < self.OTHER:
            return self.getLineVert(tx,ty)
        if not diag:
            return []
        deltax = max(tx) - min(tx)
        deltay = max(ty) - min(ty)
        assert (deltax == deltay)
        tx = (self.coord2[0]-self.coord1[0])/abs(self.coord2[0]-self.coord1[0])
        ty = (self.coord2[1]-self.coord1[1])/abs(self.coord2[1]-self.coord1[1])
        ret = []
        for inc in range(0,deltax+1):
            ret.append ((self.coord1[0]+inc*tx,self.coord1[1]+inc*ty))
        return ret
    def getLineVert(self,tx,ty):
        ret = []
        for x in range(min(tx),max(tx)+1):
            for y in range(min(ty),max(ty)+1):
              ret.append((x,y))
        # print (self.coord1, self.coord2, ret)
        return ret
    def __str__(self):
        return  "%s -> %s" % (self.coord1,self.coord2)


with open(f, "r") as fh:
    lines = [x.strip() for x in fh.readlines()]

lines = [Line(x) for x in lines]

c=collections.Counter()
c2=collections.Counter()
for l in lines:
    for p in l.getLine():
        c[p]+=1
    for p in l.getLine(diag = True):
        c2[p]+=1

print (sum((1 for k,v in c.items() if v>1)))
print (sum((1 for k,v in c2.items() if v>1)))

