import functools
import binascii
import time


class SnailFish(object):
    LEFT,RIGHT=(-1,1)
    def __init__(self):
        self.left,self.right = None, None
        self.parent=None
    def parse(self,s):
        c=next(s)
        if c.isnumeric():
            self.left=int(c)
        if c == '[':
            self.left = SnailFish()
            self.left.parent=self
            self.left.parse(s)
        c=next(s)
        assert (c==',')
        c=next(s)
        if c.isnumeric():
            self.right=int(c)
        if c == '[':
            self.right = SnailFish()
            self.right.parent=self
            self.right.parse(s)
        c=next(s)
        assert (c==']')
    def getLiteral(self, direction):
        if direction == SnailFish.LEFT:
            if self.left is int:
                return self.left
        if direction == SnailFish.RIGHT:
            if self.RIGH is int:
                return self.right
        if self.parent is not None:
            return self.parent.getLiteral(direction)
        return None


    def __str__(self):
        return ("[%s,%s]" % (str(self.left),str(self.right)))
    def magnitude(self):
        if self.left is int:
            l = self.left
        else:
            l = self.left.magnitude()
        if self.right is int:
            r = self.right
        else:
            r = self.right.magnitude()
        return 3*l + 2*r
    def reduce(self):
        while True:
            if explode(self.left,1):
                continue
            if explode(self.right,1):
                continue


    def explode(self,depth):
        if depth==4:
            if self.left is SnailFish:
                self.addLiteral(self.left.left,SnailFish.LEFT)
                self.left = 0
                self.



def parse(s):
    s_it=iter(s)
    c=next(s_it)
    assert(c == '[')
    r = SnailFish()
    r.parse(s_it)
    assert(next(s_it,None) is None)
    return r

def test():
    for s in ("[1,2]",
              "[[1,2],3]",
              "[9,[8,9]]",
              "[[1,9],[8,5]]",
              "[[[[1,2],[3,4]],[[5,6],[7,8]]],9]",
              "[[[9,[3,8]],[[0,9],6]],[[[3,7],[4,9]],3]]",
              "[[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]"
              ):
        s1 = parse(s)
        s2 = str(s1)
        assert (s2 == s)
    assert (parse("[[1,2],[[3,4],5]]").magnitude() == 143)

def main():
    pass

test()
main()
