if False:
    f="day4_test.txt"
else:
    f="day4.txt"


class EndOfFile(Exception):
    pass


cards=[]

def read_sequence(fh):
    return [int(x) for x in fh.readline().strip().split(",")]

class Bingo(object):
    def  __init__(self, fh):
        self.SIZE = 5
        self.lines = []
        while True:
            self.addLine(fh.readline())
            if len(self.lines) == self.SIZE:
                return

    def addLine(self, line):
        if line == '':
            raise EndOfFile

        c = [int(x) for x in line.strip().split(" ") if x!='']
        
        if len(c) == self.SIZE:
            self.lines.append(c)

    def __str__(self):
        return "\n".join ([' '.join((str(y) for y in x)) for x in self.lines])

    def is_in_grid(self, number):
        for x in range(0,self.SIZE):
            r = self.lines[x]
            for y in range(0, self.SIZE):
                if r[y] == number:
                    return (x,y)
        return None

class Play(object):
    def __init__(self, card):
        self.card = card
        self.filled = set()
    def play (self, x):
        a = self.card.is_in_grid(x)
        if a:
            self.filled.add(a)
            return self.winner()
        return False
    def winner(self):
        for x in range(0, self.card.SIZE):
            l = True
            c = True
            for y in range(0, self.card.SIZE):
                l = l and ( (x,y) in self.filled )
                c = c and ( (y,x) in self.filled )
            if c or l:
                return True
        return False
    

    def remaining_sum(self):
        s = 0
        for x in range(0, self.card.SIZE):
            for y in range (0, self.card.SIZE):
                if (x,y) not in self.filled:
                    s += self.card.lines[x][y]
        return s




class BingoGame(object):
    def __init__(self,cards):
        self.games = {Play(x) for x in cards}
    def star1(self, seq):
        for number in seq:
            for g in self.games:
                if g.play(number):
                    return number*g.remaining_sum()
        return None
    def star2(self, seq):
        for number in seq:
            winners=set()
            for g in self.games:
                if g.play(number):
                    winners.add(g)
            for w in winners:
                self.games.remove (w)
            if len(self.games) == 0:
                return number*w.remaining_sum()





with open(f, "r") as fh:
    seq=read_sequence(fh)

    while (fh):
        try:
            cards.append(Bingo(fh))
        except EndOfFile:
            break

b=BingoGame(cards)
print (b.star1(seq))
print (b.star2(seq))

