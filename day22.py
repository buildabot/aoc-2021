import functools
import binascii
import time
import os.path
import datetime
import logging

day = os.path.basename(__file__).split('.')[0]
logging.basicConfig(format='%(asctime)s - %(levelname)s:%(message)s',
                    level=logging.DEBUG,
                    filename="%s.log"%(day,),
                    filemode="w")

class Instruction(object):
    def __init__(self,line):
        action, coord = line.split(' ')
        x,y,z=coord.split(',')
        def parse(foo):
            return [int(x) for x in foo[2:].split('..')]

        self.x = parse(x)
        self.y = parse(y)
        self.z = parse(z)
        self.action = {'on':1,'off':0}[action]
    def getCubes(self,m):
        logging.debug("X:(%s,%s) Y:(%s,%s) Z:(%s,%s)",self.x[0],self.x[1],self.y[0],self.y[1],self.z[0],self.z[1])
        ret = set()
        for x in range(max(self.x[0], -m), min(self.x[1],m)+1):
            for y in range(max(self.y[0], -m), min(self.y[1],m)+1):
                for z in range(max(self.z[0], -m), min(self.z[1],m)+1):
                    ret.add((x,y,z))
        logging.debug("%s cube(s)",len(ret))
        assert(len(ret) == 0 or len(ret)==(abs(self.x[1]-self.x[0])+1)*(abs(self.y[1]-self.y[0])+1)*(abs(self.z[1]-self.z[0])+1))
        return ret


def processInstructions(m, instr):
    c =set()
    for i in instr:
        c1 = i.getCubes(m)
        if i.action == 1: #on
            c |= c1
        else:
            c -= c1
    return c


def loadFile(f):
    ret = []
    with open(f,'r') as fh:
        for x in fh.readlines():
            ret.append(Instruction(x.strip()))
    return ret

def test():
    a = Instruction("on x=10..12,y=10..12,z=10..12")
    c = a.getCubes(50)
    assert (len(c) == 27)

    a = Instruction("on x=-54112..-39298,y=-85059..-49293,z=-27449..7877")
    c = a.getCubes(50)
    assert (len(c) == 0)

    a = Instruction("on x=967..23432,y=45373..81175,z=27513..53682")
    c = a.getCubes(50)
    assert (len(c) == 0)

    i = [Instruction(x) for x in ["on x=10..12,y=10..12,z=10..12","on x=11..13,y=11..13,z=11..13","off x=9..11,y=9..11,z=9..11","on x=10..10,y=10..10,z=10..10"]]
    c = processInstructions(50,i)
    assert(len(c) == 39)


    logging.debug("starting test vector")
    f = "%s_test.txt" % (day,)
    instructions = loadFile(f)
    c = processInstructions(50, instructions)
    assert(len(c) == 590784)

def main():
    f = "%s.txt" % (day,)
    instructions = loadFile(f)
    c = processInstructions(50, instructions)
    print ("star 1: %s" % (len(c),))


def run():
    t1 = datetime.datetime.now()
    test()
    t2 = datetime.datetime.now()
    logging.debug("test took %s" % (str(t2-t1)))
    main()
    t3 = datetime.datetime.now()
    logging.debug ("run took %s" % (str(t3-t2)))


run()
