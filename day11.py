import functools
import binascii
import time


class Octopus(object):
    def __init__(self, energy):
        self.energy = energy
        self.has_flashed = False
    def increment_energy(self):
        self.energy += 1
    def flash(self):
        if self.energy > 9 and not self.has_flashed:
            self.has_flashed = True
            return True
        return False
    def reset(self):
        if self.has_flashed:
            self.has_flashed = False
            self.energy = 0
            return True
        return False

class Grid(object):
    def __init__(self):
        self.data = None
        self.size_x = 0
        self.size_y = 0
        self.flashes = 0
    def parse(self, data):
        self.data = []
        for line in data:
            self.data.append ([Octopus(int(x)) for x in line])
            self.size_y = len(self.data[-1])
        self.size_x = len(self.data)
    def flash(self):
        retVal = False
        for x in range (0,self.size_x):
            for y in range(0,self.size_y):
                r = self.data[x][y].flash()
                if r:
                    self.flashes+=1
                    for xx,yy in ((1,0),(1,1),(0,1),(-1,1),(-1,0),(-1,-1),(0,-1),(1,-1)):
                        try:
                            if x+xx < 0:
                                continue
                            if y+yy < 0:
                                continue
                            self.data[x+xx][y+yy].increment_energy()
                        except IndexError:
                            pass
                    retVal = True
        return retVal
    def iterate(self):
        # increment energy
        for x in range (0,self.size_x):
            for y in range(0,self.size_y):
                self.data[x][y].increment_energy()
        #flash
        while self.flash():
            pass
        # reset
        ret = True
        for x in range (0,self.size_x):
            for y in range(0,self.size_y):
                ret = self.data[x][y].reset() and ret
        return ret
    def __str__(self):
        return "%s\n" % ('\n'.join([''.join ([str(y.energy) for y in x]) for x in self.data]),)


def grid_from_file(f):
    with open(f,"r") as fh:
        data = [x.strip() for x in fh.readlines() if len(x)>0]
    g = Grid()
    g.parse(data)
    return g


def test():
    g = Grid()
    g.parse(["11111","19991","19191","19991","11111"])
    print (str(g),g.flashes)
    g.iterate()
    print (str(g),g.flashes)
    g.iterate()
    print (str(g),g.flashes)
    g2 = grid_from_file('day11_test.txt')
    print (str(g2))
    for x in range(0,10):
        g2.iterate()
        print ("%s\n%s\n" % (x+1,str(g2)))
    print (str(g2),g2.flashes)
    assert (g2.flashes == 204)

    g2 = grid_from_file('day11_test.txt')
    n=0
    while True:
        n+=1
        if g2.iterate():
            break
        if n ==200:
            break
        if n== 195:
            print (str(g2))
    print(str(g2))
    assert (n==195)

def main():
    g = grid_from_file("day11.txt")
    for x in range(0,100):
        g.iterate()
    print ("star 1 : %u" % (g.flashes))

    g = grid_from_file("day11.txt")
    n=0
    while True:
        n+=1
        if g.iterate():
            break
    print ("star 2 : %u" % (n))

test()
main()
